<?php

class WCDC_Advanced_Hooks {

	/**
	 * Stores the content generated in the set_iterations method
	 *
	 * (default value: array())
	 *
	 * @var array
	 * @access public
	 */
	var $iterations = array();

	/**
	 * The copy provided to the constructor that will be iterated in an HTML comment.
	 *
	 * (default value: '')
	 *
	 * @var string
	 * @access public
	 */
	var $copy       = '';

	/**
	 * Number of times to iterate the copy into an HTML comment.
	 *
	 * (default value: 100)
	 *
	 * @var int
	 * @access public
	 */
	var $count_down;

	/**
	 * Sets the $copy and $count properties.
	 ( Calls the set_iterations and add_actions methods
	 *
	 * @access public
	 * @param  string $copy
	 * @return void
	 */
	function __construct( $copy = '', $count = 1000 ) {
		$this->copy       = $copy;
		$this->count_down = $count;
		$this->set_iterations();
		$this->add_action();
	}

	/**
	 * Sets the $iterations property.
	 *
	 * @access public
	 * @return void
	 */
	function set_iterations() {

		$count_up = 0;

		while ( $this->count_down ) {
			$this->iterations[] = sprintf( '<!-- WCDC Iterative Output %s "%s" %s remaining-->%s', ++$count_up, $this->copy, --$this->count_down, PHP_EOL );
		}

	}

	/**
	 * Adds the actions to output iterations.
	 *
	 * @access public
	 * @return void
	 */
	function add_action() {
		add_action( 'wp_print_footer_scripts'   , array( $this, 'print_footer_scripts' ) );
		add_action( 'admin_print_footer_scripts', array( $this, 'print_footer_scripts' ) );
	}

	/**
	 * Callback on `wp_print_footer_scripts` and `admin_print_footer_scripts` hooks.
	 * Outputs the content of the $iterations property.
	 *
	 * @access public
	 * @return void
	 */
	function print_footer_scripts() {
		foreach ( $this->iterations as $iteration ) {
			echo $iteration;
		}
	}
}

new WCDC_Advanced_Hooks( 'The class.WCDC_Advanced_Hooks.php file was loaded' );
