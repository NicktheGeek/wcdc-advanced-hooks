<?php

/**
 * Registers the wcdc_advanced_hooks CPT.
 * We won't be using a WCDC_Advanced_Hooks object here because this should always be loaded.
 * However, we will be using an advanced option in the registration args to allow simplifying some loading.
 */
class WCDC_Advanced_Hooks_CPT {

	var $cpt = 'wcdc_advanced_hooks';

	/**
	 * Registers the `wcdc_advanced_hooks` CPT.
	 *
	 * @access public
	 * @return void
	 */
	function register_post_type() {

		$labels = array(
			'name'               => _x( 'Advanced Hooks'        , 'wcdc_advanced_hooks general name' , 'wcdc-advanced-hooks' ),
			'singular_name'      => _x( 'Advanced Hook'         , 'wcdc_advanced_hooks singular name', 'wcdc-advanced-hooks' ),
			'add_new'            => _x( 'Add New Advanced Hook' , 'wcdc_advanced_hooks'              , 'wcdc-advanced-hooks' ),
			'menu_name'          => __( 'Advanced Hooks'                                             , 'wcdc-advanced-hooks' ),
			'add_new_item'       => __( 'Add New Advanced Hook'                                      , 'wcdc-advanced-hooks' ),
			'edit_item'          => __( 'Edit Advanced Hook'                                         , 'wcdc-advanced-hooks' ),
			'new_item'           => __( 'New Advanced Hook'                                          , 'wcdc-advanced-hooks' ),
			'all_items'          => __( 'All Advanced Hooks'                                         , 'wcdc-advanced-hooks' ),
			'view_item'          => __( 'View Advanced Hook'                                         , 'wcdc-advanced-hooks' ),
			'search_items'       => __( 'Search Advanced Hooks'                                      , 'wcdc-advanced-hooks' ),
			'not_found'          => __( 'No Advanced Hooks Found'                                    , 'wcdc-advanced-hooks' ),
			'not_found_in_trash' => __( 'No Advanced Hooks Found In Trash'                           , 'wcdc-advanced-hooks' ),
			'parent_item_colon'  => '',
		);

		$description = __( 'This is a sample post type used to demonstrate advanced hook techniques for adding post meta', 'wcdc-advanced-hooks' );

		$args = array(
			'labels'               => $labels,
			'description'          => $description,
			'public'               => true,
			'show_ui'              => true,
			'capability_type'      => 'page',
			'has_archive'          => false,
			'hierarchical'         => false,
			'supports'             => array( 'title' ),
			'menu_position'        => 73.42,
			//we'll use this feature later, but for now it is commented out
			//'register_meta_box_cb' => array( 'WCDC_Advanced_Hooks_Post_Meta', 'meta_box_cb' ),
		);

		register_post_type( $this->cpt, $args );

	}

}
