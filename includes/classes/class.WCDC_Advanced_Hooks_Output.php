<?php

class WCDC_Advanced_Hooks_Output {

	/**
	 * Callback on `wp_enqueue_scripts'.
	 * No need to check if it is the CPT because that was checked on template_redirect already.
	 *
	 * @access public
	 * @return void
	 */
	function wp_enqueue_scripts() {
		WCDC_Advanced_Hooks_Init()->WCDC_Advanced_Hooks_Scripts_Styles->enqueue_script( 'wpdc-ah-front-end-js'  );
		WCDC_Advanced_Hooks_Init()->WCDC_Advanced_Hooks_Scripts_Styles->enqueue_style(  'wpdc-ah-front-end-css' );
	}

	/**
	 * Callback on `the_content` filter.
	 * No need to check the post type because we did that on template_redirect.
	 *
	 * @access public
	 * @param  string $content
	 * @return string
	 */
	function the_content( $content ) {

		$options = get_option( 'wcdc-advanced-hooks' );
		if ( get_post_type() === WCDC_Advanced_Hooks_Init()->WCDC_Advanced_Hooks_CPT->cpt && ! empty( $options['wcdc-advanced-hooks-enable'] ) ) {

			$content = get_post_meta( get_the_id(), 'wcdc-my-post-meta', true );

		}

		return $content;

	}

}

new WCDC_Advanced_Hooks( 'The class.WCDC_Advanced_Hooks_Output.php file was loaded' );
