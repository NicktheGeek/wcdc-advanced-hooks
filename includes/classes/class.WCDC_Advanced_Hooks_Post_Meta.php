<?php

/**
 * Adds the post meta for the advanced hooks cpt.
 * Example Methods for 3 different ways of adding the meta box are included.
 * Only one method should be used so the unused methods will be removed in the final.
 *
 * All the methods are static because of how they are invoked.
 * For more realistic options we'd use the static method to instantiate a new object that will actually generate the meta output.
 * It would also be helpful to have views that will handle the HTML so we can focus integration testing on the logic instead of the HTML.
 */
class WCDC_Advanced_Hooks_Post_Meta {

	/**
	 * Callback on the admin_eqnueue_scripts hook.
	 * Enqueues the script/style for this post type.
	 *
	 * @access public
	 * @static
	 * @return void
	 */
	static function enqueue_scripts() {
		WCDC_Advanced_Hooks_Init()->WCDC_Advanced_Hooks_Scripts_Styles->enqueue_script( 'wpdc-ah-post-meta-js'  );
		WCDC_Advanced_Hooks_Init()->WCDC_Advanced_Hooks_Scripts_Styles->enqueue_style(  'wpdc-ah-post-meta-css' );
	}

	/**
	 * Callback on `add_meta_boxes`.
	 * We can make this more specific using `add_meta_boxes_{post_type}.
	 * We can also use the register_post_type `register_meta_box_cb` to make the code even more streamlined.
	 *
	 * @access public
	 * @static
	 * @param string $post_type
	 * @param object $post
	 * @return void
	 */
	static function add_meta_boxes( $post_type, $post ) {

		new WCDC_Advanced_Hooks( 'The add_meta_boxes() method was called' );

		if ( WCDC_Advanced_Hooks_Init()->WCDC_Advanced_Hooks_CPT->cpt !== $post_type ) {
			return;
		}

		WCDC_Advanced_Hooks_Post_Meta::add_meta_box();

	}
	/**
	 * Callback on `add_meta_boxes_{post_type}`.
	 * We can use the register_post_type `register_meta_box_cb` to make the code even more streamlined.
	 *
	 * @access public
	 * @static
	 * @param object $post
	 * @return void
	 */
	static function add_meta_boxes_post_type( $post ) {

		new WCDC_Advanced_Hooks( 'The add_meta_boxes_post_type() method was called' );

		WCDC_Advanced_Hooks_Post_Meta::add_meta_box();

	}

	/**
	 * Callback on the register_post_type register_meta_box_cb argument.
	 *
	 * @access public
	 * @static
	 * @param mixed $post
	 * @return void
	 */
	static function meta_box_cb( $post ) {

		new WCDC_Advanced_Hooks( 'The meta_box_cb() method was called' );

		WCDC_Advanced_Hooks_Post_Meta::add_meta_box();

	}

	/**
	 * Adds the metabox for the advanced hooks CPT.
	 *
	 * @access public
	 * @static
	 * @return void
	 */
	static function add_meta_box() {
		add_meta_box(
			'wcdc_advanced_hooks_post_meta_box',
			__( 'WCDC Advanced Hooks' ),
			array( 'WCDC_Advanced_Hooks_Post_Meta', 'meta_box' ),
			WCDC_Advanced_Hooks_Init()->WCDC_Advanced_Hooks_CPT->cpt,
			'normal',
			'default'
		);
	}

	/**
	 * Callback for the add_meta_box function.
	 * Outputs the wcdc post meta option.
	 *
	 * @access public
	 * @static
	 * @param obj $post
	 * @return void
	 */
	static function meta_box( $post ) {

		wp_nonce_field( WCDC_AH_DIR, 'wcdc_post_meta_nonce' );

		$id = 'wcdc-my-post-meta';

		printf(
			'<p class="wcdc-ah-pm"><label for="%1$s">%3$s</label>: <input type="text" id="$1$s" name="%1$s" value="%2$s" placeholder="%4$s" /></p>',
			$id,
			esc_html( get_post_meta( $post->ID, $id, true ) ), //don't trust even though we santize on save
			__( 'WCDC Meta Option', 'wcdc-advanced-hooks' ),
			__( 'Enter a Value'   , 'wcdc=advanced-hooks' )
		);

	}
}

new WCDC_Advanced_Hooks( 'The class.WCDC_Advanced_Hooks_Post_Meta.php file was loaded' );
