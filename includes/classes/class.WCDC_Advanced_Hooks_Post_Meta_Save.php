<?php

/**
 * Handles the meta for the Advanced Hooks CPT.
 * The save_post action is very generic so to speed things up we will want to use post type specific hooks.
 *
 * The set/sanitize/update_meta methods could really be simplified into a single method. Breaking it up just makes it easier to test each component.
 *
 * For more complicated plugins this could be abstracted so the properties are updated with the correct values to simplify the code base and avoid duplication. (DRY code)
 */
class WCDC_Advanced_Hooks_Post_Meta_Save {

	/**
	 * The $POST to check for the nonce key
	 *
	 * (default value: 'wcdc_post_meta_nonce')
	 *
	 * @var string
	 * @access public
	 */
	var $nonce_field  = 'wcdc_post_meta_nonce';
	/**
	 * nonce_action
	 *
	 * (default value: WCDC_AH_DIR)
	 *
	 * @var mixed
	 * @access public
	 */
	var $nonce_action = WCDC_AH_DIR;
	/**
	 * The meta values to sanitize and update
	 *
	 * (default value: array())
	 *
	 * @var array
	 * @access public
	 */
	var $meta         = array();

	/**
	 * Key/Sanitize function pairs.
	 *
	 * This dictionary defines pairs with the $_POST key to check and the function name that should be used to sanitize that data.
	 * Some helpful functions to use https://codex.wordpress.org/Validating_Sanitizing_and_Escaping_User_Data#Sanitizing:_Cleaning_User_Input
	 *
	 * @var array
	 * @access public
	 */
	var $keys         = array(
		'wcdc-my-post-meta' => 'sanitize_text_field',
	);

	/**
	 * Post ID for the current post being udpated
	 *
	 * @var int
	 * @access public
	 */
	var $post_id;

	/**
	 * Callback on the `save_post_{post_type}` action.
	 *
	 * Since there is a redirect that happens on save, it doesn't help to output the WCDC_Advanced_Hooks strings here.
	 *
	 * @access public
	 * @static
	 * @param int $post_id
	 * @param obj $post
	 * @return void
	 */
	static function save_post(  $post_id, $post ) {
		//uses static so it can be extended.
		new static(  $post_id, $post );
	}

	/**
	 * Checks the nonce to make sure this is legit.
	 * Then checks if the user can actually edit the post.
	 * Sets the $post_id property
	 * Invokes methods to set, sanitize, and update meta.
	 *
	 * @access public
	 * @param  int $post_id
	 * @param  obj $post
	 * @return void
	 */
	function __construct( $post_id, $post ) {

		if ( empty( $_POST[$this->nonce_field] ) || ! wp_verify_nonce( $_POST[$this->nonce_field], $this->nonce_action ) ) {
			return;
		}

		$post_type = get_post_type_object( $post->post_type );

		/* Check if the current user has permission to edit the post. */
		if ( ! current_user_can( $post_type->cap->edit_post, $post_id ) ){
			return;
		}

		$this->post_id = $post_id;

		$this->set_meta();
		$this->sanitize_meta();
		$this->update_meta();

	}

	/**
	 * Sets the $meta property.
	 *
	 * @access public
	 * @return void
	 */
	function set_meta() {

		foreach ( $this->keys as $key => $sanitize_function ) {

			if ( isset( $_POST[$key] ) ) {
				$this->meta[$key] = $_POST[$key];
			}

		}

	}

	/**
	 * Sanitizes values in the $meta property.
	 *
	 * @access public
	 * @return void
	 */
	function sanitize_meta() {

		foreach ( $this->meta as $key => $value ) {
			$this->meta[$key] = $this->keys[$key]( $value );
		}

	}

	/**
	 * Updates values from the $meta property.
	 *
	 * @access public
	 * @return void
	 */
	function update_meta() {

		foreach ( $this->meta as $key => $value ) {
			update_post_meta( $this->post_id, $key, $value );
		}

	}
}

new WCDC_Advanced_Hooks( 'The class.WCDC_Advanced_Hooks_Post_Meta_Save.php file was loaded' );
