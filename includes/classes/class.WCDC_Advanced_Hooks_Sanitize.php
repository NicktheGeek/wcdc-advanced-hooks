<?php

/**
 * Sanitizes the provided inputs.
 * This can be extended so that adding different values in the sanitize_callbacks properties will allow different items to be sanitized without needing a whole new class.
 */
class WCDC_Advanced_Hooks_Sanitize {

	/**
	 * The input values to be sanitized
	 *
	 * @var mixed
	 * @access public
	 */
	var $input;

	/**
	 * State boolean used to determin if the inputs have been sanitized or not
	 *
	 * (default value: false)
	 *
	 * @var bool
	 * @access public
	 */
	var $sanitized = false;

	/**
	 * Registered callbacks for input keys.
	 * Some helpful functions to use https://codex.wordpress.org/Validating_Sanitizing_and_Escaping_User_Data#Sanitizing:_Cleaning_User_Input
	 *
	 * @var mixed
	 * @access public
	 */
	var $sanitize_callbacks = array(
		'wcdc-advanced-hooks-enable' => 'intval',
	);

	/**
	 * Constructor.
	 * Sets the $input property
	 *
	 * @access public
	 * @param  array $input
	 * @return void
	 */
	function __construct( $input ) {

		$this->input = $input;

	}

	/**
	 * Gets the $input property after ensuring it is santized.
	 *
	 * @access public
	 * @return array
	 */
	function get_sanitized_input() {

		if ( empty( $this->sanitized ) ) {
			$this->sanitize_input();
		}

		return $this->input;

	}

	/**
	 * Santizes the input property.
	 * Sets the $sanitized property to true after sanitizing.
	 *
	 * @access public
	 * @return void
	 */
	function sanitize_input() {

		if ( $this->input ) {
			foreach ( $this->input as $key => $val ) {

				if ( isset( $this->sanitize_callbacks[$key] ) ) {
					$this->input[$key] = $this->sanitize_callbacks[$key]( $val );
				}

			}
		}

		$this->sanitized = true;

	}

}

new WCDC_Advanced_Hooks( 'The class.WCDC_Advanced_Hooks_Sanitize.php file was loaded' );
