<?php

/**
 * WCDC_Advanced_Hooks_Scripts_Styles class.
 */
class WCDC_Advanced_Hooks_Scripts_Styles {

	/**
	 * Registers the scripts.
	 * This is registering admin and front end scripts so it could be broken out to load only admin or only front end with a seperate method to handle the other.
	 *
	 * @access public
	 * @return void
	 */
	function register_scripts() {
		wp_register_script( 'wpdc-ah-admin-js'    , WCDC_AH_ASSETS . 'js/admin.js'    , array( 'jquery' ), 0.1, true );
		wp_register_script( 'wpdc-ah-post-meta-js', WCDC_AH_ASSETS . 'js/post-meta.js', array( 'jquery' ), 0.1, true );
		wp_register_script( 'wpdc-ah-front-end-js', WCDC_AH_ASSETS . 'js/front-end.js', array( 'jquery' ), 0.1, true );
	}

	/**
	 * Registers the styles.
	 * This is registering admin and front end styles so it could be broken out to load only admin or only front end with a seperate method to handle the other.
	 *
	 * @access public
	 * @return void
	 */
	function register_styles() {
		wp_register_style( 'wpdc-ah-admin-css'    , WCDC_AH_ASSETS . 'css/admin.css'    , false, 0.1 );
		wp_register_style( 'wpdc-ah-post-meta-css', WCDC_AH_ASSETS . 'css/post-meta.css', false, 0.1 );
		wp_register_style( 'wpdc-ah-front-end-css', WCDC_AH_ASSETS . 'css/front-end.css', false, 0.1 );
	}
	
	/**
	 * Enqueues the script.
	 * This isn't expressly required because it just wraps the wp_enqueue_script function
	 * but lets us see when it is being invoked and the impact more clearly because of the WCDC_Advanced_Hooks object.
	 * 
	 * @access public
	 * @param  string $id
	 * @return void
	 */
	function enqueue_script( $id ) {
		new WCDC_Advanced_Hooks( 'The enqueue_script() method was called' );
		wp_enqueue_script( $id );
	}
	/**
	 * Enqueues the style.
	 * This isn't expressly required because it just wraps the wp_enqueue_style function
	 * but lets us see when it is being invoked and the impact more clearly because of the WCDC_Advanced_Hooks object.
	 * 
	 * @access public
	 * @param  string $id
	 * @return void
	 */
	function enqueue_style( $id ) {
		new WCDC_Advanced_Hooks( 'The enqueue_style() method was called' );
		wp_enqueue_style( $id );
	}

}
