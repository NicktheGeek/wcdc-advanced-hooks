<?php

/**
 * Outputs the settings page content
 *
 * In a more advanced plugin it would be a good idea to use "views" to handle the HTML. This class could be done as an object and make the object extendible.
 * Then multiple admin pages can use the same parent class and views. This makes for DRY code and more importantly it means it is a lot easier to change the HTML markup
 * because the HTML markup is divorced from the actual settings. This approach also makes it a lot easier to test code so a lot of good reasons to do it that way from the start.
 */
class WCDC_Advanced_Hooks_Settings_Page {

	/**
	 * Callback on the load-{hook} action.
	 * Adds the enqueue action for this specific settings page.
	 * Uses the "static" entity to simplify extending this class.
	 *
	 * @access public
	 * @static
	 * @return void
	 */
	static function load() {
		add_action( 'admin_enqueue_scripts', array( get_class(), 'enqueue_scripts' ) );
	}

	/**
	 * Callback on admin_enqueue_scripts hook.
	 * Enqueues the admin JS and CSS using WCDC_Advanced_Hooks_Scripts_Styles methods.
	 * Normally you would use wp_enqueue_script/style directly but this is demonstrating load and potential conflicts.
	 *
	 * @access public
	 * @static
	 * @return void
	 */
	static function enqueue_scripts() {
		WCDC_Advanced_Hooks_Init()->WCDC_Advanced_Hooks_Scripts_Styles->enqueue_script( 'wpdc-ah-admin-js'  );
		WCDC_Advanced_Hooks_Init()->WCDC_Advanced_Hooks_Scripts_Styles->enqueue_style(  'wpdc-ah-admin-css' );
	}

	/**
	 * Callback for the wcdc_advanced_hooks menu page.
	 * Generates HTML output for the wcdc_advanced_hooks page
	 *
	 * @access public
	 * @static
	 * @return void
	 */
	static function wcdc_settings_page() {
		// check user capabilities
		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}

		// add error/update messages

		// check if the user have submitted the settings
		// wordpress will add the "settings-updated" $_GET parameter to the url
		if ( isset( $_GET['settings-updated'] ) ) {
			// add settings saved message with the class of "updated"
			add_settings_error( 'wporg_messages', 'wporg_message', __( 'Settings Saved', 'wcdc-advanced-hooks' ), 'updated' );
		}

		// show error/update messages
		settings_errors( 'wporg_messages' );
?>
		<div class="wrap">
			<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
			<form action="options.php" method="post">
				<?php
		// output security fields for the registered setting "wporg"
		settings_fields( 'wcdc-advanced-hooks' );
		// output setting sections and their fields
		do_settings_sections( 'wcdc-advanced-hooks' );
		// output save settings button
		submit_button( __( 'Save Settings', 'wcdc-advanced-hooks' ) );
?>
			</form>
		</div>
		<?php
	}

	/**
	 * Callback for the `wcdc_settings_section` section.
	 * Generates HTML output for the wcdc_settings_section section.
	 *
	 * @access public
	 * @static
	 * @param mixed $args
	 * @return void
	 */
	static function wcdc_settings_section( $args ) {
?>
		<p id="<?php echo esc_attr( $args['id'] ); ?>"><?php esc_html_e( 'The Section Callback.', 'wcdc-advanced-hooks' ); ?></p>
		<?php
	}

	/**
	 * Callback for the `wcdc-advanced-hooks-enable` settings field.
	 * Generates output for `wcdc-advanced-hooks-enable` settings field
	 *
	 * @access public
	 * @param  array $args
	 * @return void
	 */
	static function wcdc_advanced_hooks_enable( $args ) {
		// get the value of the setting we've registered with register_setting()
		$options = get_option( 'wcdc-advanced-hooks' );
		$option  = empty( $options[$args['label_for']] ) ? 0 : 1;
		// output the field
?>
		<input type="checkbox" class="<?php esc_attr( $args['class'] ); ?>" id="<?php echo esc_attr( $args['label_for'] ); ?>" name="wcdc-advanced-hooks[<?php echo esc_attr( $args['label_for'] ); ?>]" value="1" <?php checked( 1, $option, true ); ?> />

		<?php if ( isset( $args['description'] ) ) : ?>
		<p class="description"><?php echo $args['description']; ?></p>
		<?php endif; ?>
		<?php
	}

}

new WCDC_Advanced_Hooks( 'The class.WCDC_Advanced_Hooks_Settings.php file was loaded' );
