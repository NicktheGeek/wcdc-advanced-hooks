<?php

/**
 * Registers the settings and admin page
 */
class WCDC_Advanced_Hooks_Settings_Register {
	
	/**
	 * Callback on admin_init hook.
	 * Registers the settings for the settings page
	 *
	 * @access public
	 * @static
	 * @return void
	 */
	static function settings_init() {

		// register a new setting for "wporg" page
		register_setting( 'wcdc-advanced-hooks', 'wcdc-advanced-hooks', array( 'WCDC_Advanced_Hooks_Settings_Register', 'sanitize_callback' ) );

		// register a new section in the "wporg" page
		add_settings_section(
			'wcdc_settings_section',
			__( 'WCDC Setting Section.', 'wcdc-advanced-hooks' ),
			array( 'WCDC_Advanced_Hooks_Settings_Page', 'wcdc_settings_section' ),
			'wcdc-advanced-hooks'
		);

		// register a new field in the "wporg_section_developers" section, inside the "wcdc_advanced_hooks" page
		add_settings_field(
			'wcdc-advanced-hooks-enable', // as of WP 4.6 this value is used only internally
			// use $args' label_for to populate the id inside the callback
			__( 'Enable WCDC', 'wcdc-advanced-hooks' ),
			array( 'WCDC_Advanced_Hooks_Settings_Page', 'wcdc_advanced_hooks_enable' ),
			'wcdc-advanced-hooks',
			'wcdc_settings_section',
			[
			'label_for'   => 'wcdc-advanced-hooks-enable',
			'class'       => 'wcdc-ah-checkbox',
			'type'        => 'checkbox',
			'description' => __( 'This is a description', 'wcdc_advanced_hooks_enable' ),
			]
		);

	}
	
	/**
	 * Callback for the Sanitize Settings.
	 *
	 * @access public
	 * @static
	 * @param mixed $input
	 * @return void
	 */
	static function sanitize_callback( $input ) {
		$sanitize = new WCDC_Advanced_Hooks_Sanitize( $input );

		return $sanitize->get_sanitized_input();
	}

	/**
	 * Callback on the admin_menu hook.
	 * Registers the settings page.
	 *
	 * @access public
	 * @static
	 * @return void
	 */
	static function admin_menu() {
		// add top level menu page
		$page_hook = add_menu_page(
			__( 'WCDC Advanced Hooks', 'wcdc-advanced-hooks' ),
			__( 'WCDC Advanced Hooks', 'wcdc-advanced-hooks' ),
			'manage_options',
			'wcdc_advanced_hooks',
			array( 'WCDC_Advanced_Hooks_Settings_Page', 'wcdc_settings_page' )
		);
		
		//we will need to load the page hook for our method to load scripts and styles more selectively
		add_action( 'load-' . $page_hook, array( 'WCDC_Advanced_Hooks_Settings_Page', 'load' ) );

	}

}

//since this file actually needs to be loaded we are going to comment this line, but the callbacks that aren't needed all the time get moved to the WCDC_Advanced_Hooks_Settings_Page object to make things more effecient.
#new WCDC_Advanced_Hooks( 'The class.WCDC_Advanced_Hooks_Settings_Register.php file was loaded' );
