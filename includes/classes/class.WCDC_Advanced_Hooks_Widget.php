<?php

/**
 * Generates the example widget.
 * This class will be loaded all over the place but we don't need all the logic here
 * so we'll break it out into widget and form specific classes to make the code properly abstract and minimize how much is loaded in a given page.
 *
 * @extends WP_Widget
 */
class WCDC_Advanced_Hooks_Widget extends WP_Widget {
	/**
	 * Default instance values.
	 *
	 * @since 2.3.4
	 *
	 * @var array
	 */
	public $defaults = array();

	/**
	 * Default constructor
	 *
	 * @since 2.3.4
	 */
	function __construct() {
		$widget_options = array(
			'classname'   => 'wcdc-ah-widget',
			'description' => __( 'A sample Widget for WCDC Advanced Hooks.', 'wcdc-advanced-hooks' ),
		);

		$control_options = array(
			'width'   => 200,
			'height'  => 250,
			'id_base' => 'wcdc-ah-widget'
		);

		$this->defaults = array(
			'show' => 0,
		);

		parent::__construct(
			'wcdc-ah-widget',
			__( 'WCDC Advanced Hooks' , 'wcdc-advanced-hooks' ),
			$widget_options,
			$control_options
		);
	}

	/**
	 * Display the widget on the front-end.
	 *
	 * Override of WP_Widget::widget().
	 *
	 * @since 2.3.4
	 *
	 * @param  array $args   An array of widget arguments.
	 * @param  array $instance  An array of widget settings.
	 * @return void
	 */
	public function widget( $args, $instance ) {
		new WCDC_Advanced_Hooks_Widget_Output( $args, $instance, $this );
	}

	/**
	 * Display the widget form.
	 *
	 * Override of WP_Widget::form().
	 *
	 * @since 2.3.4
	 *
	 * @param  array $instance An array of widget settings.
	 * @return void
	 */
	public function form( $instance ) {
		new WCDC_Advanced_Hooks_Widget_Settings( $instance, $this );
	}

}
