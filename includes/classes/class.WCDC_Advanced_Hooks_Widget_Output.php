<?php

/**
 * Generates the example widget.
 * This class will be loaded all over the place but we don't need all the logic here
 * so we'll break it out into widget and form specific classes to make the code properly abstract and minimize how much is loaded in a given page.
 *
 * @extends WP_Widget
 */
class WCDC_Advanced_Hooks_Widget_Output {
	/**
	 * WCDC_Advanced_Hooks_Widget object.
	 *
	 * @since 0.5
	 *
	 * @var obj
	 */
	var $WCDC_Advanced_Hooks_Widget;
	/**
	 * WCDC_Advanced_Hooks_Widget current instance.
	 *
	 * @since 0.5
	 *
	 * @var array
	 */
	var $instance;

	/**
	 * Default constructor
	 *
	 * @since 2.3.4
	 */
	function __construct( $args, $instance, $WCDC_Advanced_Hooks_Widget ) {
		$this->WCDC_Advanced_Hooks_Widget = $WCDC_Advanced_Hooks_Widget;
		$this->instance                   = wp_parse_args( (array) $instance, $this->WCDC_Advanced_Hooks_Widget->defaults );

		$this->widget( $args );
	}

	/**
	 * Display the widget on the front-end.
	 *
	 * Override of WP_Widget::widget().
	 *
	 * @since 2.3.4
	 *
	 * @param  array $args   An array of widget arguments.
	 * @param  array $instance  An array of widget settings.
	 * @return void
	 */
	public function widget( $args ) {
		if ( $this->instance['show'] ) {
			printf( '<p>%s</p>', __( 'Look like there is something to show', 'wcdc-advanced-hooks' ) );
		}
	}

}
