<?php

/**
 * Generates the example widget.
 * This class will be loaded all over the place but we don't need all the logic here
 * so we'll break it out into widget and form specific classes to make the code properly abstract and minimize how much is loaded in a given page.
 *
 * @extends WP_Widget
 */
class WCDC_Advanced_Hooks_Widget_Settings {

	/**
	 * WCDC_Advanced_Hooks_Widget_Settings object.
	 *
	 * @since 2.11
	 *
	 * @var obj
	 */
	var $WCDC_Advanced_Hooks_Widget_Settings;
	/**
	 * WCDC_Advanced_Hooks_Widget_Settings current instance.
	 *
	 * @since 2.11
	 *
	 * @var array
	 */
	var $instance;

	/**
	 * Default constructor
	 *
	 * @since 2.3.4
	 */
	function __construct( $instance, $WCDC_Advanced_Hooks_Widget_Settings ) {

		$this->WCDC_Advanced_Hooks_Widget_Settings = $WCDC_Advanced_Hooks_Widget_Settings;
		$this->instance              = wp_parse_args( (array) $instance, $this->WCDC_Advanced_Hooks_Widget_Settings->defaults );

		$this->form();

	}

	/**
	 * Display the widget form.
	 *
	 * Override of WP_Widget::form().
	 *
	 * @since 2.3.4
	 *
	 * @param  array $instance An array of widget settings.
	 * @return void
	 */
	public function form() {
		$this->check_box_option( 'show', __( 'Show something', 'wcdc-advanced-hooks' ) );
	}

	/**
	 * Outputs a default checkbox option.
	 *
	 * @access public
	 * @param mixed $id
	 * @param mixed $label
	 * @return void
	 */
	function check_box_option( $id, $label ) {
?>
		<p class="<?php echo $id; ?>">
			<input id="<?php echo $this->WCDC_Advanced_Hooks_Widget_Settings->get_field_id( $id ); ?>" type="checkbox" name="<?php echo $this->WCDC_Advanced_Hooks_Widget_Settings->get_field_name( $id ); ?>" value="1" <?php checked( $this->instance[$id] ); ?> />
			<label for="<?php echo $this->WCDC_Advanced_Hooks_Widget_Settings->get_field_id( $id ); ?>"><?php echo $label; ?></label>
		</p>
		<?php
	}

}
