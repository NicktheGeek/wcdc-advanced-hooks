# WordCamp DC 2017 Advanced WordPress Hooks Demo Plugin

This is a demo plugin for the WCDC 2017 Efficient plugin design using advanced WordPress hooks presentation. The content is organized into branches so that the stages of the plugin can be viewed. It is also organized by release tags. This should allow for exploring the code in ways that are more comfortable for the reader.

Commit history, merge history, branches, and release tags all contain the same basic information. Release tags include more notes for each improvement.

This plugin includes common features such as a custom post type, custom post meta, a settings page, scripts and styles. It does not include custom tables.

In general best practices are being followed at the time of the plugin creation. This include sanitizing content on save and escaping on output to ensure it is generally speaking "safe." Code standards are following a slightly modified version of WordPress code standards to ensure compatibility with any plugin or theme following WordPress code standards. 

The plugin is using different methods as examples showing alternate methods of achieving similar means. A good rule of thumb would be to use one method for a given plugin, but as this is for demonstration multiple methods are being used.

The code is Object Oriented (OOP) but many of the methods could be adapted to more static programing instead. It will require an extra layer to implement with static functions over objects as the autoloader will not work. One method demonstrated is use of a class that is strictly a static library. For developers new to OOP this may be the ideal way to get started as it isn't really using objects, just a class with static methods.
