<?php
/*
Plugin Name: WCDC Advanced Hooks
Plugin URI: http://designsbynickthegeek.com
Description: Sample Plugin for Advanced WordPress Hooks
Version: 0.4
Author: Nick the Geek
Author URI: http://designsbynickthegeek.com
*/

if ( ! defined( 'ABSPATH' ) ) {
	die( "Sorry, you are not allowed to access this page directly." );
}


/**
 * Initializes the plugin.
 *
 * Adds required actions/filters.
 * Uses an autoloader to load files as needed automatically.
 */
class WCDC_Advanced_Hooks_Init {

	var $WCDC_Advanced_Hooks_CPT;
	var $WCDC_Advanced_Hooks_Scripts_Styles;
	var $WCDC_Advanced_Hooks_Output;

	/**
	 * The files and associated classes used by the plugin.
	 *
	 * @var array
	 * @access public
	 */
	static $classes = array(
		'WCDC_Advanced_Hooks',
		'WCDC_Advanced_Hooks_CPT',
		'WCDC_Advanced_Hooks_Output',
		'WCDC_Advanced_Hooks_Post_Meta_Save',
		'WCDC_Advanced_Hooks_Post_Meta',
		'WCDC_Advanced_Hooks_Settings_Page',
		'WCDC_Advanced_Hooks_Settings_Register',
		'WCDC_Advanced_Hooks_Sanitize',
		'WCDC_Advanced_Hooks_Scripts_Styles',
		'WCDC_Advanced_Hooks_Widget',
		'WCDC_Advanced_Hooks_Widget_Output',
		'WCDC_Advanced_Hooks_Widget_Register',
		'WCDC_Advanced_Hooks_Widget_Settings',
	);

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	function __construct() {

		//since this is loaded on init if we are doing it right, we will want to start the register post type object right away
		$this->register_post_type();

		$this->add_actions_filters();

	}

	/**
	 * Registers all the actions/filters.
	 *
	 * @access public
	 * @return void
	 */
	function add_actions_filters() {

		$this->WCDC_Advanced_Hooks_Scripts_Styles = new WCDC_Advanced_Hooks_Scripts_Styles;

		//switching to add_meta_boxes_{post_type} makes this more effecient since it won't load on other post types
		add_action( 'add_meta_boxes_' . $this->WCDC_Advanced_Hooks_CPT->cpt, array( 'WCDC_Advanced_Hooks_Post_Meta', 'add_meta_boxes_post_type' ), 10, 2 );

		//Switching to use the save_post_{post_type} action is more effecient even if it isn't currently documented. You can find it in the wp-includes/post.php file using grep for the save_post hook
		add_action( 'save_post_' . $this->WCDC_Advanced_Hooks_CPT->cpt     , array( 'WCDC_Advanced_Hooks_Post_Meta_Save', 'save_post'      ), 10, 2 );

		//this does need to be on admin_init so it can be registered throughout the admin settings. It might be possible to use some clever code to check the request and the pagehook but you gain little so not worth it.
		add_action( 'admin_init', array( 'WCDC_Advanced_Hooks_Settings_Register' , 'settings_init' ) );

		//this really does have to be on all admin pages so it can show up in the menu so no need to be clever with it ever.
		add_action( 'admin_menu', array( 'WCDC_Advanced_Hooks_Settings_Register' , 'admin_menu'    ) );

		//replacing the_content filter with an action on template_redirect let's us check the post type before adding the content filter
		add_action( 'template_redirect', array( $this, 'pre_the_content' ) );

		//this will load on all pages so we know the scripts are registered if needed
		add_action( 'wp_enqueue_scripts'   , array( $this->WCDC_Advanced_Hooks_Scripts_Styles, 'register_scripts' ), -1 );
		add_action( 'wp_enqueue_scripts'   , array( $this->WCDC_Advanced_Hooks_Scripts_Styles, 'register_styles'  ), -1 );
		add_action( 'admin_enqueue_scripts', array( $this->WCDC_Advanced_Hooks_Scripts_Styles, 'register_scripts' ), -1 );
		add_action( 'admin_enqueue_scripts', array( $this->WCDC_Advanced_Hooks_Scripts_Styles, 'register_styles'  ), -1 );

		//instead of always enqueueing scripts and styles, we'll move the logic to another action that lets us target page hooks. It is also possible to check the hook suffix in wp_enqueue_script argument
		//The admin script/style is moved to register on the load-{page_hook} in the WCDC_Advanced_Hooks_Settings_Page class
		//The post meta script/style is added on the load-post.php and load-post-new.php with a post type check. At this time it is the most effecient way to do this
		//Multiple post types with different script/style needs could be handled in a single object but since this is super simple we'll just use this object for the load method
		add_action( 'load-post.php'    , array( $this, 'load_post' ) );
		add_action( 'load-post-new.php', array( $this, 'load_post' ) );

	}

	/**
	 * Instantiates the WCDC_Advanced_Hooks_CPT object.
	 *
	 * @access public
	 * @return void
	 */
	function register_post_type() {

		$this->WCDC_Advanced_Hooks_CPT = new WCDC_Advanced_Hooks_CPT;

		$this->WCDC_Advanced_Hooks_CPT->register_post_type();

	}

	/**
	 * Callback on `load-post.php` and `load-post-new.php` actions.
	 * Checks the post type then adds the enqueue_scripts action.
	 * The $post value isn't setup yet, so we've got to get a bit hackish to check the post type this early.
	 *
	 * @access public
	 * @return void
	 */
	function load_post() {
		$post_type = isset( $_GET['post']      ) ? get_post_type( $_GET['post'] ) : '';
		$post_type = isset( $_GET['post_type'] ) ? $_GET['post_type']             : $post_type;

		if ( $post_type === $this->WCDC_Advanced_Hooks_CPT->cpt ) {
			add_action( 'admin_enqueue_scripts', array( 'WCDC_Advanced_Hooks_Post_Meta', 'enqueue_scripts' ) );
		}

	}

	/**
	 * Callback on `template_redirect` hook.
	 * Checks the post type before adding the_content filter and the enqueue action.
	 * Unlike the previous method, this instantiates the WCDC_Advanced_Hooks_Output so which allows it to behave as an object.
	 * This would be very helpful for more complicated solutions that may need to build the output.
	 *
	 * @access public
	 * @return void
	 */
	function pre_the_content() {

		if ( get_post_type() === $this->WCDC_Advanced_Hooks_CPT->cpt ) {

			$this->WCDC_Advanced_Hooks_Output = new WCDC_Advanced_Hooks_Output;

			add_action( 'wp_enqueue_scripts', array( $this->WCDC_Advanced_Hooks_Output, 'wp_enqueue_scripts' ) );
			add_filter( 'the_content'       , array( $this->WCDC_Advanced_Hooks_Output, 'the_content'        ) );

		}

	}
	
	/**
	 * Callback on `widgets_init` action.
	 * Registers the example widget.
	 * 
	 * @access public
	 * @return void
	 */
	static function widgets_init() {
		register_widget( 'WCDC_Advanced_Hooks_Widget' );
	}

	/**
	 * Registered autoload function.
	 * Used to load class files automatically if they are in the provided array.
	 *
	 * @access public
	 * @param mixed $class
	 * @return void
	 */
	static function autoloader( $class ) {

		if ( ! in_array( $class, WCDC_Advanced_Hooks_Init::$classes ) ) {
			return;
		}

		include( sprintf( '%sclass.%s.php', WCDC_AH_CLASSES, $class ) );

	}

}

add_action( 'init', 'WCDC_Advanced_Hooks_Init' );

//bonus registering a widget to demonstrate a method on minimizing code load
//Since widgets_init is called on all pages, we can just use the init object
add_action( 'widgets_init', array( 'WCDC_Advanced_Hooks_Init', 'widgets_init' ) );

//widgets_init loads before init so we'll move the autoloader to outside the object
spl_autoload_register( array( 'WCDC_Advanced_Hooks_Init', 'autoloader' ) );

//we also need the constants
define( 'WCDC_AH_DIR'    , plugin_dir_path( __FILE__ )                       );
define( 'WCDC_AH_CLASSES', WCDC_AH_DIR                 . 'includes/classes/' );
define( 'WCDC_AH_ASSETS' , plugin_dir_url( __FILE__ )  . 'assets/'           );

/**
 * Callback on the `init` hook.
 * Initializes and returns the WCDC_Advanced_Hooks_Init object.
 *
 * @access public
 * @return obj
 */
function WCDC_Advanced_Hooks_Init() {
	global $WCDC_Advanced_Hooks_Init;

	if ( empty( $WCDC_Advanced_Hooks_Init ) ) {
		$WCDC_Advanced_Hooks_Init = new WCDC_Advanced_Hooks_Init;
	}

	return $WCDC_Advanced_Hooks_Init;
}
